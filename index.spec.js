describe('index.js', () => {
  it('prints something to console', () => {
    let outputData = '';
    const log = inputs => (outputData += inputs);
    console['log'] = jest.fn(log);
    require('./index.js');
    expect(outputData).toBe('🚀');
  });
});
